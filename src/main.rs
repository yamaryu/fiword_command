use clap::Parser;
use std::path::PathBuf;
use std::io::BufReader;
use std::env;
use std::fs::File;
use std::io::prelude::*;
use std::fs;
use std::io;
use std::path::Path;

#[derive(Parser)]
#[clap(
    name = "fiword",
    author = "Ryuta Yamaguchi",
    version = "0.1.0",
    about = "search for files that contain specfic strings in the document"
)]

struct Options {
    #[clap(short='m', long="multiword", value_name = "SEARCH_MULTI_WORD", help="Search multi words in the current directory.")]
    directory: Option<PathBuf>,
}

fn main() -> std::io::Result<()> {
    let path = env::current_dir()?;
    let args: Vec<String> = env::args().collect();
    //println!("The current directory is {}", path.display());
    read_dir(args, path); // 検索する文字
    Ok(())
}

fn read_dir(args:Vec<String>, path:PathBuf) {
    let target = "./";
    let mut files = Vec::new();
    for path in fs::read_dir(target).unwrap() {
        files.push(path.unwrap().path().display().to_string().replacen(target, "", 1))
    }

    files.sort();
    let mut strings = files.iter()
        .fold(String::new(), |joined, s| {
            if joined == String::new() { s.to_string() } else { joined + "  " + s }
        });
    let mut test = strings.split_whitespace();
    //println!("{}", args[1]);
    
    //println!("The current directory is {}", path.display());

    for s in test {
        let mut count = 0;
        if s.contains(&args[1]){
            count = count_up(count);
            //println!("{}", s);
        }
        if args.len() == 3 {
            if s.contains(&args[2]) {
                count = count_up(count);
            }
        }

        if count > 0 {
            println!("{}", s);
        }
    }
}

fn count_up(mut count: i32) -> i32{
    count += 1;
    return count;
}


fn hello(name: Option<String>) -> String {
    return format!("Hello, {}", if let Some(n) = name {
        n
    } else {
        "World".to_string()
    })
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_basic() {
        assert_eq!("Hello, World", hello(None));
        assert_eq!("Hello, Yamaguchi", hello(Some("Yamaguchi".to_string())));
        assert_eq!(2, count_up(1));
        assert_eq!(4, count_up(3));
        assert_eq!(0, count_up(-1));
    }
}