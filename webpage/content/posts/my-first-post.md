[![Rust Report Card](https://rust-reportcard.xuri.me/badge/gitlab.com/yamaryu/fiword_command)](https://rust-reportcard.xuri.me/report/gitlab.com/yamaryu/fiword_command)

# fiword_command

特定の文字列を引数として、その文字列を文書内に含むファイルの検索をするコマンドツール

## fiword

特定の文字列を引数として、その文字列を文書内に含むファイルの検索をするコマンドツール

## Description

このツールは文字列をファイル内に含むファイルを探すツールである。検索対象のディレクトリを指定することやディレクトリ配下全てを検索対象とすることも可能である。

## Usage

```
fiword [OPTIONS] <SEARCH DIR> <SEARCH WORD>
OPTIONS
    -r, --recursive <search dir> 配下の全てのファイルを検索する
    -h, --help このメッセージを表示
SEARCH DIR
    探すファイルの保存場所
SEARCH WORD
    探す文字列

```

## Sample Output

```
$ fiword ./ hoge
test.txt /home/User/test.txt
main.rs /home/User/main.rs

$ fiword ~/Desktop/ hoge
Not Fount.

$fiword -r ~/Downloads/ hogehoge
test.txt /home/User/Downloads/test.txt
sub.rs /home/User/Downloads/Rust/sub.rs
```
